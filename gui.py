#!/usr/bin/env python3
import sys
import logging
import tkinter as tk
from pathlib import Path
from tkinter import ttk
from tkinter import filedialog as tkfd
from tkinter import simpledialog as tksd
from tkinter import messagebox as tkmb

# import bulk_renamer_t1 as brrt1
brrt1 = __import__('bulk-renamer-rt1')


logger = logging.getLogger(__name__)


TITLE = 'BioTools'


def get_string(prompt=None):
    return tksd.askstring(title=TITLE, prompt=prompt)


class GUI(ttk.Frame):
    def __init__(self, master):
        super().__init__(master)
        self.pack()

        ACTIONS = {
            'Bulk Rename': self.bulk_rename,
        }

        # Create widgets
        self.title = ttk.Label(self, text='Select the task')
        self.b = {}
        for label, action in ACTIONS.items():
            self.b[label] = ttk.Button(self, text=label,
                                       command=self.do_action(action))
        self.exit = ttk.Button(self, text='Exit', command=self.master.destroy)
        # Layout widgets
        self.title.pack(side=tk.TOP)
        self.exit.pack(side=tk.BOTTOM)
        for w in self.b.values():
            w.pack(side=tk.BOTTOM)

    def do_action(self, fn):
        def action(event=None):
            if fn():
                tkmb.showinfo(title=TITLE,
                              message='All Done!')
            else:
                tkmb.showerror(title=TITLE,
                               message='Errors, check terminal!')
        return action

    # Actions
    def bulk_rename(self):
        folder = Path(tkfd.askdirectory(mustexist=True))
        logger.debug('Selected Directory: %s', folder)
        return brrt1.work(folder,
                          get_string=get_string)


def entrypoint():
    logging.basicConfig(level=logging.DEBUG,
                        format='%(levelname)5.5s[%(module)s] %(message)s')
    root = tk.Tk()

    gui = GUI(root)
    gui.mainloop()
    return 0  # OK


if __name__ == '__main__':
    sys.exit(entrypoint())
