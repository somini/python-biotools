#!/usr/bin/env python3
import sys
import argparse
import logging
from collections import defaultdict
from pathlib import Path


logger = logging.getLogger(__name__)


def work(folder, *, get_string=input):
    torename = defaultdict(list)
    for f in folder.iterdir():
        if f.is_file():
            stem = f.name.split('.', maxsplit=1)[0]
            logger.debug('> %s: %s' % (stem, f.name))
            torename[stem].append(f)

    logger.info('Found %d file blocks to rename' % len(torename))

    for stem, files in torename.items():
        newstem = get_string('==> Rename "%s" to: ' % stem)
        if newstem and len(newstem) > 0:
            logger.info('    Renamed!')
            for f in files:
                f.rename(f.with_stem(newstem))
        else:
            logger.info('    Skipped')

    logger.info('All done!')
    return True


def function(args):
    parser = argparse.ArgumentParser(description='Bulk Rename Files')
    parser.add_argument('--folder', type=Path,
                        default=Path('.'),
                        help='The folder to process')
    parser.add_argument('--verbose', '-v', action='store_const',
                        const=logging.DEBUG, default=logging.INFO,
                        help='Show debug logs')
    args = parser.parse_args(args)

    logging.basicConfig(level=args.verbose,
                        format='%(levelname)5.5s[%(module)s] %(message)s')

    return 0 if work(args.folder) else 1


def entrypoint():
    return function(sys.argv[1:])


if __name__ == '__main__':
    sys.exit(entrypoint())
